from bottle import route, run, template, static_file, request, redirect
import sqlite3, time, unicodedata

address = 'localhost'

#serve static files
@route('/static/<filename:path>')
def st(filename):
    return static_file(filename, root='static/')

#index page route
@route('/')
def indexFunction():
    conn = sqlite3.connect('discurseData.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("SELECT id, tag, type, title, content FROM postData ORDER BY postTime DESC")
    result = c.fetchall()
    c.close()
    return template('index.tpl', data = result, addr = address)


#get request for the textpost submission page
@route('/submitText', method="get")
def submitTextFunctionGet():
    return template('submitText.tpl')

#post for above
@route('/submitText', method="post")
def submitTextFunctionPost():
    title = request.forms.get('title').strip()
    title.decode('unicode-escape')
    title = unicode(title, 'utf-8')
    tag = request.forms.get('tag').strip()
    content = request.forms.get('content').strip()
    content.decode('unicode-escape')
    content = unicode(content, 'utf-8')
    contentType = "textPost"
    postTime = int(time.time())
    if(type(tag)=='str'):
        conn = sqlite3.connect('discurseData.db')
        c = conn.cursor()
        c.execute("INSERT INTO postData (type, title, content, tag, postTime) values (?,?,?,?,?)", (contentType, title, content, tag, postTime))
        conn.commit()
        c.close()
        redirect('/')
    else:
        return template('message.tpl', message = "Tags cannot have unicode characters, sorry 'bout that. :/")

#get request for the linkpost submission page
@route('/submitLink', method="get")
def submitLinkFunctionGet():
    return template('submitLink.tpl')

#post for above
@route('/submitLink', method="post")
def submitLinkFunctionPost():
    title = request.forms.get('title').strip()
    title.decode('unicode-escape')
    title = unicode(title, 'utf-8')
    tag = request.forms.get('tag').strip()
    content = request.forms.get('content').strip()
    content.decode('unicode-escape')
    content = unicode(content, 'utf-8')
    contentType = "linkPost"
    postTime = int(time.time())
    if(type(tag)=='str' or type(content)=='str'):
        conn = sqlite3.connect('discurseData.db')
        c = conn.cursor()
        c.execute("INSERT INTO postData (type, title, content, tag, postTime) values (?,?,?,?,?)", (contentType, title, content, tag, postTime))
        conn.commit()
        c.close()
        redirect('/')
    else:
        return template('message.tpl', message = "Tags or links cannot be unicode characters. Sorry 'bout that.")

#route for each individual post link
@route('/<no:int>')
def getPost(no):
    conn = sqlite3.connect('discurseData.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("SELECT * FROM postData WHERE id LIKE ?", (no, ))
    result = c.fetchone()
    c.execute("SELECT content, id from commentData WHERE postid LIKE ? ORDER BY commentTime DESC", (no, ))
    commentResult = c.fetchall()
    c.close()
    return template('post.tpl', data = result, commentData = commentResult, addr = address)

#post request for above, used for posting comments
@route('/<no:int>', method="post")
def submitComment(no):
    conn = sqlite3.connect('discurseData.db')
    content = request.forms.get('comment').strip()
    commentTime = int(time.time())
    c = conn.cursor()
    c.execute("INSERT INTO commentData (content, commentTime, postId) values (?,?,?)", (content, commentTime, no))
    conn.commit()
    c.close()
    redirect('/'+str(no))

#get request for each individual tag
@route('/tags/<tag>')
def getPostByTag(tag):
    conn = sqlite3.connect('discurseData.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("SELECT id, tag, type, title, content FROM postData WHERE tag like ? ORDER BY postTime DESC", (tag,))
    result = c.fetchall()
    c.close()
    return template('index.tpl', data = result, addr = address)

#get request for the login page
@route('/login')
def loginPage():
    return template('login.tpl')

@route('/register')
def registerPage():
    return template('register.tpl')

run(host=address, port=8080, reload = True)


