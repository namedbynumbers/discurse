## Discurse
A simple reddit clone with slight adjustments written in bottlepy. Mostly done as a project to learn about Python and Webapp development.

Features of discurse:

    1. No account required to post links and text posts.
    2. No account required to post comments.
    3. Content is classified based on tags rather than separate subreddits. (Unimplemented)
    4. There are 3 different kinds of upvotes (Agreed, Insightful, Funny). No downvotes. Voting requires an account. (Unimplemented)