% include('base.tpl')
<link rel="stylesheet" href="/static/css/index.css">
<div id="content">
%for row in data:
    <div class="post_div">
        %if(row['type']=="textPost"):
        <p class="post_title">({{row['id']}})(@<a href="/tags/{{row['tag']}}">{{row['tag']}}</a>)<a href="/{{row['id']}}">{{row['title']}}</a></p>
        %elif(row['type']=="linkPost"):
            <p class="post_title">({{row['id']}})(@<a href="/tags/{{row['tag']}}">{{row['tag']}}</a>)<a href="{{row['content']}}">{{row['title']}}</a></p>
        %end
        <p class="post_links"><a href="/{{row['id']}}">permalink</a></p>
        
    </div>
%end
</div>