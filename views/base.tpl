<!DOCTYPE html>
<html lang="en">

<head>
    <!--<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>-->
    <title>disc#rse</title>
    <link rel="stylesheet" href="/static/css/base.css">
</head>

<body>
    <p id="title">/disc#rse/<span id="subhead">let's talk</span></p>

    <div id="navbar">
        <span class="nav_button">
            <a href="/" class="nav_link">home</a>
        </span>
        <span class="nav_button">
           <a href="#" class="nav_link">new</a>
        </span>
        <span class="nav_button">
            <a href="/submitText" class="nav_link">submit text</a>
        </span>
        <span class="nav_button">
            <a href="/submitLink" class="nav_link">submit link</a>
        </span>
        <span class="nav_button">
            <a href="/login" class="nav_link">login</a>
        </span>
        <span class="nav_button">
            <a href="/register" class="nav_link">register</a>
        </span>
    </div>

    
</body>

</html>
