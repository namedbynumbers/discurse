% include('base.tpl')
<link rel="stylesheet" href="/static/css/post.css">
<div id="content">
    <div class="post_div">
        <div>({{data['id']}})(@{{data['tag']}})({{data['title']}})</div>
        <pre id="post_content">{{data['content']}}</pre>
    </div>
    <hr>
    <div id="comment_box">
        <form action="/{{data['id']}}" method="post">
            <textarea id="comment_textarea" name = "comment" rows="5" required></textarea><br>                       <input type="submit" name="submit" value="Submit">
        </form>
    </div>
    <hr>
    <div id="comment_section">
        %for row in commentData:
            <div class="comment_div">
                >>{{row['id']}} <br>
                <p>
                {{row['content']}} <br>
                </p>
            </div>
        %end
    </div>
            
</div>